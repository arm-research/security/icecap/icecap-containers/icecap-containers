## Build your own linux kernel

To build a linux kernel compatable with IceCap: 

Clone `linux` repo: 

```bash
git clone --recursive https://gitlab.com/arm-research/security/icecap/linux.git
cd linux 
make distclean
make ARCH=arm64 defconfig
make ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu-gcc
```