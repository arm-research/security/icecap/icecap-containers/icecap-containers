# Standalone OCI container in IceCap

Steps to Run a standalone OCI compliant container image, for example a docker `hello-world` image, in Icecap:

### Creating Icecap Host and Guest Rootfs and initrd:

We use `Dockerfiles` to build custom host and guest VMs. Follow this guide to [install Docker](https://docs.docker.com/engine/install/) if necessary.

Once all the necessary requirements are available, run the following commands:
- Clone the Icecacp repository:

    ```bash
    git clone --recursive -b dev https://gitlab.com/arm-research/security/icecap/icecap-containers/icecap-containers.git
    ```
- Once you have a local copy of the Icecap source, run the following to generate the host and guest rootfs and initrd.

    ```bash
    cd vm-build
    ./create.sh all --guest-file Dockerfile.guest --host-file Dockerfile.host
    ```
#### Custom Guest VM:

To Customize the guest VM, simply change [the guest Dockerfile](../vm-build/Dockerfile.guest) and rebuild the guest initrd using the script above.

In case the new custom guest needs additional/different kernel configuration, follow this [guide to build a new kernel](build-kernel.md). Once the build is done, modify this line in the [default.nix](../nix/instance/default.nix) to point to the location of the new kernel.

```bash
spec = mkLinuxRealm {
    kernel = local_path_to_kernel;
    initrd = local_path_to_inirtd;
    ...
  };
```


### To build and run Icecap:
```bash
$(machine) cd icecap
$(machine) pushd hack/nix
$(machine) nix-build
$(machine) popd
$(machine) export PATH="$(pwd)/hack/nix/result/bin:$PATH"
$(machine) cd ..
$(machine) nix-build nix/ -A virt.run
$(machine) ./result/run
# ... wait for the Icecap host VM to boot to a shell ...
# .. you should see something like this
# make sure we're logged into Icecap-host
$(Icecap-host) hostname
    Icecape_host
# Spawn a VM in a realm 
$(Icecap-host) ic
# ... wait for the Icecap guest VM to boot to a shell ...
# .. you should see something like this
# The host VM uses virtual console 0, and the guest VM uses virtual console 1.
# Switch to the guest VM virtual console by typing '@1'
$(Icecap-guest) hostname
    Icecape_guest
```
### Run hello-world container inside an Icecap Realm:

```bash
# To generate a config file for runc and start the container
$(Icecap-guest) runc spec
# This will produce a config.json file
# for the purpose of this demo, you need to change 2 lines in config.json file to 
     "terminal": fasle,
     ...
     "args": [
              "/hello"

# save the config.json file and run
$(Icecap-guest) runc run --no-pivot test
# This will successful create an OCI complaint hello-worl container inside an Icecap guest, the output should be
```
>   
    Hello from Docker!
    This message shows that your installation appears to be working correctly.

    To generate this message, Docker took the following steps:
    1. The Docker client contacted the Docker daemon.
    2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
        (arm64v8)
    3. The Docker daemon created a new container from that image which runs the
        executable that produces the output you are currently reading.
    4. The Docker daemon streamed that output to the Docker client, which sent it
        to your terminal.

    To try something more ambitious, you can run an Ubuntu container with:
    $ docker run -it ubuntu bash

    Share images, automate workflows, and more with a free Docker ID:
    https://hub.docker.com/

    For more examples and ideas, visit:
    https://docs.docker.com/get-started/


```bash
# 'ctrl-a x' quits QEMU
```